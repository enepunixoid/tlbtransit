unit TLBConfig;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils,INIFiles;

type _config = record
     global_log:boolean;
     Company_Company:string;
     Company_DefaultUser:string;
     Company_DefaultCity:string
     SysAdmin_Password:string;
  end;

  { tConfig }

  tConfig = class(TObject)
    public
      ini:_config;
      inifile:string;
      inif:TINIFile;
      constructor Create(f:string);
      destructor Destroy;
      procedure SaveConfig;
      procedure LoadConfig;
  end;

implementation

{ tConfig }

constructor tConfig.Create(f: string);
begin
   self.inifile:=f;
   self.inif:=TINIFile.Create(t);
   self.LoadConfig;
end;

destructor tConfig.Destroy;
begin

end;

procedure tConfig.SaveConfig;
begin
   self.inif.WriteBool('GLOBAL','Log',self.ini.global_log);
   self.inif.WriteBool('COMPANY','Company',self.ini.Company_Company);
   self.inif.WriteBool('CITY','DefaultUser',self.ini.Company_DefaultUser);
   self.inif.WriteBool('USER','DefaultCity',self.ini.Company_DefaultCity);
   self.inif.WriteBool('SYSADMIN','Password',self.ini.SysAdmin_Password);

end;

procedure tConfig.LoadConfig;
begin
   self.ini.global_log:=self.inif.ReadBool('GLOBAL','Log');
   self.ini.Company_Company:=self.inif.ReadString('COMPANY','Company');
   self.ini.Company_DefaultUser:=self.inif.ReadString('COMPANY','DefaultUser');
   self.ini.Company_DefaultCity:=self.inif.ReadString('COMPANY','DefaultCity');
   self.ini.SysAdmin_Password:=self.inif.ReadString('SYSADMIN','Password');

end;

end.

