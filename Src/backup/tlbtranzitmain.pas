{%RunFlags MESSAGES+}
unit TLBTranzitMain;

{$mode objfpc}{$H+}



interface

uses
   {$IFDEF WINDOWS}
  Dos,crt,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ComCtrls,
  IniPropStorage, AnchorDockPanel,INIFiles,TBLDefs;


type

  { TFormMain }

  TFormMain = class(TForm)
    MainMenu1: TMainMenu;
    DBMItem: TMenuItem;
    HelperMItem: TMenuItem;
    EdirorDBMItem: TMenuItem;
    SaveDBMItem: TMenuItem;
    LoadDBMItem: TMenuItem;
    Separator1: TMenuItem;
    Separator2: TMenuItem;
    Separator3: TMenuItem;
    System: TMenuItem;
    VedomostiMItem: TMenuItem;
    ExitMItem: TMenuItem;
    HelpMItem: TMenuItem;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
  private
    sysAdminAccess:boolean;
    userAccess:integer;
    WorkingPath:string;
    ConfigPath:string;
    config:tConfig;


  public

  end;

var
  FormMain: TFormMain;

implementation

{$R *.lfm}

{ TFormMain }

procedure TFormMain.FormCreate(Sender: TObject);
var
  SrcFile:TStream;
  DstFile:TStream;
begin
  self.sysAdminAccess:=false;
  self.userAccess:=0;
  {$IFDEF LINUX}
     self.ConfigPath := SysUtils.GetEnvironmentVariable('HOME')+'/.config/TLB/';

  write('TLB: Config: '+ self.ConfigPath+' ... ');
  if SysUtils.DirectoryExists(self.ConfigPath) then writeln('OK')
  else
  begin
       if SysUtils.CreateDir(self.ConfigPath) then
       begin
            SrcFile:=TFileStream.Create('../local/settings.ini',fmOpenRead);
            DstFile:=TFileStream.Create( self.ConfigPath+'settings.ini',fmCreate);
            DstFile.CopyFrom(SrcFile, SrcFile.Size);

            Writeln('CREATE');
       end
       else WriteLn('FAIL');
  end;
  {$ENDIF}
  {$IFDEF WINDOWS}
     self.ConfigPath := GetEnv('userprofile')+'\.config\TLB\';

  write('TLB: Config: '+ self.ConfigPath+' ... ');
  if SysUtils.DirectoryExists(self.ConfigPath) then writeln('OK')
  else
  begin
       if SysUtils.CreateDir(self.ConfigPath) then
       begin
            SrcFile:=TFileStream.Create('..\local\settings.ini',fmOpenRead);
            DstFile:=TFileStream.Create( self.ConfigPath+'settings.ini',fmCreate);
            DstFile.CopyFrom(SrcFile, SrcFile.Size);

            Writeln('CREATE');
       end
       else WriteLn('FAIL');
  end;
  {$ENDIF}
//  self.INI.Create(self.ConfigPath);
//  writeln('INI: '+self.INI.StoredValue['password']);

end;

end.

