unit TLBSettingForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  ComCtrls, AnchorDockPanel;

type

  { TFormSettings }

  TFormSettings = class(TForm)
    AnchorDockPanel1: TAnchorDockPanel;
    AnchorDockPanel2: TAnchorDockPanel;
    Button1: TButton;
    Button2: TButton;
    GroupBox1: TGroupBox;
    title: TEdit;
    log: TCheckBox;
    global: TGroupBox;
    GroupBox2: TGroupBox;
    procedure FormCreate(Sender: TObject);
    procedure GroupBox1Click(Sender: TObject);
    procedure GroupBox2Click(Sender: TObject);
    procedure LabeledEdit2Change(Sender: TObject);
  private

  public

  end;

var
  FormSettings: TFormSettings;

implementation

{$R *.lfm}

{ TFormSettings }

procedure TFormSettings.FormCreate(Sender: TObject);
begin

end;

procedure TFormSettings.GroupBox1Click(Sender: TObject);
begin

end;

procedure TFormSettings.GroupBox2Click(Sender: TObject);
begin

end;

procedure TFormSettings.LabeledEdit2Change(Sender: TObject);
begin

end;




end.

