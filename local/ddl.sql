--
-- Файл сгенерирован с помощью SQLiteStudio v3.4.4 в Ср дек 13 17:13:52 2023
--
-- Использованная кодировка текста: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Таблица: tAddrs
DROP TABLE IF EXISTS tAddrs;

CREATE TABLE IF NOT EXISTS tAddrs (
    ID        INTEGER PRIMARY KEY ASC AUTOINCREMENT
                      UNIQUE,
    Client_ID INTEGER NOT NULL
                      REFERENCES tClients (ID),
    IndexCode INTEGER,								-- Почтовый индекс
    Street    TEXT,									-- Улица, проспект, и тд.
    HouseBody TEXT,									-- Номер дома, корпус
    Room      TEXT									-- квартира, офис, комната и помещение
);


-- Таблица: tBankInfo
DROP TABLE IF EXISTS tBankInfo;

CREATE TABLE IF NOT EXISTS tBankInfo (
    ID    INTEGER PRIMARY KEY AUTOINCREMENT,
    BIK   INTEGER,									-- БИК Банка
    Title TEXT										-- Наименование Банка
);


-- Таблица: tCargo
DROP TABLE IF EXISTS tCargo;

CREATE TABLE IF NOT EXISTS tCargo (
    ID     INTEGER PRIMARY KEY AUTOINCREMENT
                   UNIQUE,
    Weight REAL,									--
    Valume REAL
);


-- Таблица: tCheck
DROP TABLE IF EXISTS tCheck;

CREATE TABLE IF NOT EXISTS tCheck (
    ID            INTEGER PRIMARY KEY AUTOINCREMENT,
    InvoiceAmount REAL,
    Paid          INTEGER,
    Order_ID      INTEGER UNIQUE
);


-- Таблица: tCity
DROP TABLE IF EXISTS tCity;

CREATE TABLE IF NOT EXISTS tCity (
    ID    INTEGER PRIMARY KEY AUTOINCREMENT
                  UNIQUE,
    Title TEXT
);

INSERT INTO tCity (ID, Title) VALUES (1, 'Челябинск');
INSERT INTO tCity (ID, Title) VALUES (2, 'Мурманск');
INSERT INTO tCity (ID, Title) VALUES (3, 'Москва');
INSERT INTO tCity (ID, Title) VALUES (4, 'Санкт-Петербург');

-- Таблица: tClients
DROP TABLE IF EXISTS tClients;

CREATE TABLE IF NOT EXISTS tClients (
    ID             INTEGER PRIMARY KEY ASC AUTOINCREMENT
                           UNIQUE,
    Name           TEXT,
    City_ID        INTEGER,
    FizAddr_ID     INTEGER,
    BizAddr_ID,
    Contacts_ID    INTEGER,
    BankDetails_ID INTEGER,
    INN            INTEGER,
    KPP,
    CheckAccount   INTEGER UNIQUE,
    AdjustAccout   INTEGER UNIQUE
);


-- Таблица: tCompany
DROP TABLE IF EXISTS tCompany;

CREATE TABLE IF NOT EXISTS tCompany (
    ID                       INTEGER PRIMARY KEY AUTOINCREMENT,
    Title                    TEXT,
    INN                      INTEGER,
    KPP                      INTEGER,
    OGRN                     INTEGER,
    OKPO                     INTEGER,
    OKVED                    INTEGER,
    CheckAccount             INTEGER,
    AdjustAccout             INTEGER,
    Bank_ID                  INTEGER REFERENCES tBankInfo (ID),
    City_ID                  INTEGER REFERENCES tCity (ID),
    Addr_id                  INTEGER REFERENCES tAddrs (ID),
    Telehone                 TEXT,
    Email                    TEXT,
    AddressForCorrespondence TEXT
);


-- Таблица: tContacts
DROP TABLE IF EXISTS tContacts;

CREATE TABLE IF NOT EXISTS tContacts (
    ID         INTEGER PRIMARY KEY AUTOINCREMENT,
    Type       INTEGER,
    Value      TEXT,
    Clients_ID INTEGER REFERENCES tClients (ID) 
);


-- Таблица: tDate
DROP TABLE IF EXISTS tDate;

CREATE TABLE IF NOT EXISTS tDate (
    ID    INTEGER PRIMARY KEY AUTOINCREMENT,
    Day   INTEGER,
    Month INTEGER,
    Year  INTEGER
);


-- Таблица: tOrder
DROP TABLE IF EXISTS tOrder;

CREATE TABLE IF NOT EXISTS tOrder (
    ID                  INTEGER PRIMARY KEY AUTOINCREMENT,
    Date_ID             INTEGER REFERENCES tDate (ID),
    Sender_Client_ID    INTEGER REFERENCES tClients (ID),
    Recipient_Cliend_ID INTEGER REFERENCES tClients (ID),
    Sender_Data_ID      INTEGER REFERENCES tDate (ID),
    Recipient_Date_ID   INTEGER REFERENCES tDate (ID),
    Cargo_ID            INTEGER REFERENCES tCargo (ID),
    Сheck_ID            INTEGER
);


-- Таблица: tUsers
DROP TABLE IF EXISTS tUsers;

CREATE TABLE IF NOT EXISTS tUsers (
    ID         INTEGER PRIMARY KEY AUTOINCREMENT,
    Login      TEXT    DEFAULT user,
    FIO        TEXT,
    Company_ID INTEGER,
    Password   TEXT    UNIQUE
);


-- Индекс: iCity
DROP INDEX IF EXISTS iCity;

CREATE INDEX IF NOT EXISTS iCity ON tCity (
    ID ASC,
    Title ASC
);


-- Индекс: inxBankInfo
DROP INDEX IF EXISTS inxBankInfo;

CREATE INDEX IF NOT EXISTS inxBankInfo ON tBankInfo (
    ID
);


-- Индекс: inxCargo
DROP INDEX IF EXISTS inxCargo;

CREATE INDEX IF NOT EXISTS inxCargo ON tCargo (
    ID
);


-- Индекс: inxCompany
DROP INDEX IF EXISTS inxCompany;

CREATE INDEX IF NOT EXISTS inxCompany ON tCompany (
    ID ASC
);


-- Индекс: inxContacts
DROP INDEX IF EXISTS inxContacts;

CREATE INDEX IF NOT EXISTS inxContacts ON tContacts (
    ID ASC
);


-- Индекс: inxOrder
DROP INDEX IF EXISTS inxOrder;

CREATE INDEX IF NOT EXISTS inxOrder ON tOrder (
    ID
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
